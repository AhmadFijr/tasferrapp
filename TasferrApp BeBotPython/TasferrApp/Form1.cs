using BeBotPythonLib;

namespace TasferrApp
{
    public partial class Form1 : Form
    {
        string python_path = @"BPSVE/BPSVE.exe";
        BeBotPython.SyncMode py_S = new BeBotPython.SyncMode();
        BeBotPython.LineMode py_L = new BeBotPython.LineMode();

        public Form1()
        {
            InitializeComponent();
            Tools_Py.Py_RunCache(python_path);
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            inp_key.Focus();
        }

        private async void btn_encript_Click(object sender, EventArgs e)
        {
            string MODE;
            if (inp_txt.Text == "") {
                MessageBox.Show("must you enter some text", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (rb_AES.Checked)
            {
                if (inp_key.TextLength < 16)
                {
                    MessageBox.Show("Incorrect AES key length must be 16", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                MODE = "MODE_AES";
            }
            else {
                MODE = "MODE_DES";
                if (inp_key.TextLength < 8)
                {
                    MessageBox.Show("Incorrect DES key length must be 8", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            btn_encript.Enabled = false;
            py_S.Py_RunPython(python_path,Py_Arguments: MODE);
            py_S.Py_Set_Input(inp_key.Text);
            py_S.Py_Set_Input(inp_txt.Text);
            
            await py_S.Py_WaitForExitAsync();

            if(MODE == "MODE_AES")
            {
                MessageBox.Show("The file is saved in the same program path as 'encrypted_AES.bin'", "Successfully", MessageBoxButtons.OK);
            }
            else
            {
                MessageBox.Show("The file is saved in the same program path as 'encrypted_DES.bin'", "Successfully", MessageBoxButtons.OK);
            }
            btn_encript.Enabled = true;
        }

        private void rb_AES_CheckedChanged(object sender, EventArgs e)
        {   
            if (rb_AES.Checked)
            {   
                inp_key.MaxLength = 16;
            }
            else {
                if(inp_key.TextLength > 8) {
                    inp_key.Clear();
                    inp_key.Focus();
                }
                inp_key.MaxLength = 8;
            }
        }

        private  async void btn_encript_file_Click(object sender, EventArgs e)
        {
            string MODE;
            if (rb_AES.Checked)
            {
                if (inp_key.TextLength < 16)
                {
                    MessageBox.Show("Incorrect AES key length must be 16", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                MODE = "MODE_AES";
            }
            else
            {
                MODE = "MODE_DES";
                if (inp_key.TextLength < 8)
                {
                    MessageBox.Show("Incorrect DES key length must be 8", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }

            //btn_encript.Enabled = false;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            //openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Text files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var path = openFileDialog1.FileName;
                //MessageBox.Show(path, "");
                

                btn_encript_file.Enabled = false;
                py_S.Py_RunPython(python_path, Py_Arguments: MODE+" "+ "FILE");
                py_S.Py_Set_Input(inp_key.Text);
                py_S.Py_Set_Input(path);

                await py_S.Py_WaitForExitAsync();

                if (MODE == "MODE_AES")
                {
                    MessageBox.Show("The file is saved in the same program path as 'encrypted_AES.bin'", "Successfully", MessageBoxButtons.OK);
                }
                else
                {
                    MessageBox.Show("The file is saved in the same program path as 'encrypted_DES.bin'", "Successfully", MessageBoxButtons.OK);
                }
                btn_encript_file.Enabled = true;

            }
        }
    }
}