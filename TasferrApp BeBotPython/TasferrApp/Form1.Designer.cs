﻿namespace TasferrApp
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.inp_txt = new System.Windows.Forms.TextBox();
            this.btn_encript = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.inp_key = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_encript_file = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.rb_AES = new System.Windows.Forms.RadioButton();
            this.rb_DES = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // inp_txt
            // 
            this.inp_txt.Location = new System.Drawing.Point(114, 220);
            this.inp_txt.Name = "inp_txt";
            this.inp_txt.Size = new System.Drawing.Size(332, 27);
            this.inp_txt.TabIndex = 0;
            // 
            // btn_encript
            // 
            this.btn_encript.Location = new System.Drawing.Point(238, 262);
            this.btn_encript.Name = "btn_encript";
            this.btn_encript.Size = new System.Drawing.Size(94, 29);
            this.btn_encript.TabIndex = 1;
            this.btn_encript.Text = "encript";
            this.btn_encript.UseVisualStyleBackColor = true;
            this.btn_encript.Click += new System.EventHandler(this.btn_encript_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(213, 197);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter text to encript";
            // 
            // inp_key
            // 
            this.inp_key.Location = new System.Drawing.Point(197, 154);
            this.inp_key.MaxLength = 16;
            this.inp_key.Name = "inp_key";
            this.inp_key.Size = new System.Drawing.Size(179, 27);
            this.inp_key.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(251, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Enter key";
            // 
            // btn_encript_file
            // 
            this.btn_encript_file.Location = new System.Drawing.Point(197, 297);
            this.btn_encript_file.Name = "btn_encript_file";
            this.btn_encript_file.Size = new System.Drawing.Size(162, 29);
            this.btn_encript_file.TabIndex = 5;
            this.btn_encript_file.Text = "encript From text file";
            this.btn_encript_file.UseVisualStyleBackColor = true;
            this.btn_encript_file.Click += new System.EventHandler(this.btn_encript_file_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(147, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(271, 37);
            this.label3.TabIndex = 6;
            this.label3.Text = "Encyption Algorthims";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(49, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 28);
            this.label4.TabIndex = 7;
            this.label4.Text = "Choose Algorthim:";
            // 
            // rb_AES
            // 
            this.rb_AES.AutoSize = true;
            this.rb_AES.Checked = true;
            this.rb_AES.Location = new System.Drawing.Point(242, 80);
            this.rb_AES.Name = "rb_AES";
            this.rb_AES.Size = new System.Drawing.Size(56, 24);
            this.rb_AES.TabIndex = 8;
            this.rb_AES.TabStop = true;
            this.rb_AES.Text = "AES";
            this.rb_AES.UseVisualStyleBackColor = true;
            this.rb_AES.CheckedChanged += new System.EventHandler(this.rb_AES_CheckedChanged);
            // 
            // rb_DES
            // 
            this.rb_DES.AutoSize = true;
            this.rb_DES.Location = new System.Drawing.Point(329, 80);
            this.rb_DES.Name = "rb_DES";
            this.rb_DES.Size = new System.Drawing.Size(57, 24);
            this.rb_DES.TabIndex = 9;
            this.rb_DES.Text = "DES";
            this.rb_DES.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 450);
            this.Controls.Add(this.rb_DES);
            this.Controls.Add(this.rb_AES);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_encript_file);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.inp_key);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_encript);
            this.Controls.Add(this.inp_txt);
            this.Name = "Form1";
            this.Text = "TassferrApp";
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TextBox inp_txt;
        private Button btn_encript;
        private Label label1;
        private TextBox inp_key;
        private Label label2;
        private Button btn_encript_file;
        private Label label3;
        private Label label4;
        private RadioButton rb_AES;
        private RadioButton rb_DES;
    }
}