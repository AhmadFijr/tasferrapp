from os import remove


def Get_File(name:str):
    with open(name,"rb") as opt:
        read = opt.read()
        read = read.decode("UTF-8")
    return read

def Set_File(name:str, text:str):
    with open(name,"w") as op:op.write(text)

def Delete_File(name:str):
    remove(name)    

def Set_OutputPy(string : str):
    with open("OutputPy.txt","w") as op:
            op.write(string)

def Get_InputPy():           
    read = Get_File("InputToPy.txt")
    return read;



    

