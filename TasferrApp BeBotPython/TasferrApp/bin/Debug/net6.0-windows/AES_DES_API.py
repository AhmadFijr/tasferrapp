from Crypto.Cipher import AES
from Crypto.Cipher import DES
from secrets import token_bytes

'''
with open('the-zen-of-python.txt') as f:
    contents = f.read()
    print(contents)
'''
    
def encrypt_AES(file=False):
    while True:
        key = input("enter key:").encode()
        try:
            cipher = AES.new(key, AES.MODE_EAX)
            break
        except ValueError:
            print("Incorrect AES key length must be 16")
            continue
    
    if file == True:
        path = input("enter the file path :")
        with open(path,"r") as f:
            msg = f.read()
    else:
        msg = input("enter the mesage :")
    
    
    nonce = cipher.nonce
    ciphertext , tag = cipher.encrypt_and_digest(msg.encode('utf-8'))
    #print(ciphertext)
    #return nonce, ciphertext, tag
    file_out = open("encrypted_AES.bin", "wb")
    [ file_out.write(x) for x in (cipher.nonce, tag, ciphertext) ]
    file_out.close()

def encrypt_DES(file=False):
    while True:
        key = input("enter key:").encode()
        try:
            cipher = DES.new(key, DES.MODE_EAX)
            break
        except ValueError:
            print("Incorrect AES key length must be 8")
            continue
        
    if file == True:
        path = input("enter the file path :")
        path = path.replace("\\\\","\\")
        with open(path) as f:
            msg = f.read()
    else:
        msg = input("enter the mesage :")
    
    
    nonce = cipher.nonce
    ciphertext , tag = cipher.encrypt_and_digest(msg.encode('utf-8'))
    #print(ciphertext)
    #return nonce, ciphertext, tag
    file_out = open("encrypted_DES.bin", "wb")
    [ file_out.write(x) for x in (cipher.nonce, tag, ciphertext) ]
    file_out.close()



