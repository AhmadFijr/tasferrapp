In the employment test to work with a company, I was asked to make a coding program with five algorithms:
* AES
* DE
* Caesar Chipper
* Vugeneres Chipper
* Play Fiar

Most of the algorithms were attached or found on the Internet, and I made minor modifications, prepared the structure and beautifully output the code, and developed two versions of the program:

1. The simplified version, contains only two algorithms and the encrypted text is output as a file, is only for the purpose of showing off the technologies it is built in... Built using the combination of Python and C# Python language using the BeBotPython API.

2. The full version, contains all the algorithms and outputs the encryption in the form of text and file, using Python language with tkinter