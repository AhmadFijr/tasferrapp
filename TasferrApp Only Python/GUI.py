
import tkinter as tk
from tkinter import messagebox
from tkinter import filedialog 
import EncryptionTools

root = tk.Tk()
root.title("TasferrApp")
root.geometry("600x500")


l_main = tk.Label(root,
                height=2,
                text="Encryption Algorthims",
                font=("Arial", 19)
                )

l_main.place(relx=0.5, rely=0.065, anchor=tk.CENTER)

l_choose = tk.Label(root,
        height=1,
        text="Choose Your Algorthim:",
        font=("Arial", 15)
        )

l_choose.place(relx=0.01, rely=0.12)

Mode="AES"
def selection_algorthim():
    global Mode
    global limitSizeKey_num
    Mode = radio_Algorthim.get()
    value = keyValue.get()

    if Mode == "AES":
        if len(value) > 16:
            keyValue.set("")
        limitSizeKey_num = 16

    elif Mode == "DES":
        if len(value) > 8:
            keyValue.set("")
        limitSizeKey_num = 8

    elif Mode == "CaesarChiper":
        limitSizeKey_num = 200

    elif Mode == "VigeneresCipher":
        limitSizeKey_num = 200

    elif Mode == "PlayFiar":
        limitSizeKey_num = 200

radio_Algorthim = tk.StringVar()


rb_AES = tk.Radiobutton(root,
                        text="AES",
                        variable=radio_Algorthim,
                        value="AES",
                        font=("Arial", 11),
                        command=selection_algorthim
                        )

rb_DES = tk.Radiobutton(root,
                        text="DES",
                        variable=radio_Algorthim,
                        value="DES",
                        font=("Arial", 11),
                        tristatevalue=0,
                        command=selection_algorthim
                        )

rb_CaesarChiper = tk.Radiobutton(root,
                        text="Caesar Chiper",
                        variable=radio_Algorthim,
                        value="CaesarChiper",
                        font=("Arial", 11),
                        tristatevalue=0,
                        command=selection_algorthim
                        )

rb_VigeneresCipher = tk.Radiobutton(root,
                        text="Vigeneres Cipher",
                        variable=radio_Algorthim,
                        value="VigeneresCipher",
                        font=("Arial", 11),
                        tristatevalue=0,
                        command=selection_algorthim
                        )

rb_PlayFiar = tk.Radiobutton(root,
                        text="Play Fiar",
                        variable=radio_Algorthim,
                        value="PlayFiar",
                        font=("Arial", 11),
                        tristatevalue=0,
                        command=selection_algorthim
                        )

rb_AES.place(relx=0.05, rely=0.19)
rb_DES.place(relx=0.25, rely=0.19)
rb_CaesarChiper.place(relx=0.45, rely=0.19)
rb_VigeneresCipher.place(relx=0.15, rely=0.25)
rb_PlayFiar.place(relx=0.55, rely=0.25)

l_key = tk.Label(root,
                text="Enter The Key",
                font=("Arial", 13),
                )
l_key.place(relx=0.5, rely=0.37, anchor=tk.CENTER)


limitSizeKey_num = 16
def limitSizeKey(*args):
    value = keyValue.get()
    if len(value) > limitSizeKey_num: keyValue.set(value[:limitSizeKey_num])

keyValue = tk.StringVar()
keyValue.trace('w', limitSizeKey)

inp_key = tk.Entry(root,
                textvariable=keyValue,
                justify='center',
                )
inp_key.place(relx=0.5, rely=0.425, anchor=tk.CENTER)

l_text = tk.Label(root,
                text="Enter Text To Encrypt",
                font=("Arial", 13),
                )
l_text.place(relx=0.5, rely=0.51, anchor=tk.CENTER)

inp_text_value = tk.StringVar()
inp_text = tk.Entry(root,
                width=60,
                justify='center',
                textvariable=inp_text_value
                )
inp_text.place(relx=0.5, rely=0.565, anchor=tk.CENTER)

def btn_Encryption_command():
    value_text = inp_text_value.get()
    value_key = keyValue.get()
    if value_key == "":
            messagebox.showwarning("Warning","Please write key for Encrypt")
            return
    
    if Mode == "AES":
        if len(value_key) < 16:
            print("Incorrect AES key length must be 16 >> try again..")
            messagebox.showwarning("Warning","Incorrect AES key length must be 16 >> try again..")
            return
    elif Mode == "DES":
        if len(value_key) < 8:
            print("Incorrect DES key length must be 8 >> try again..")
            messagebox.showwarning("Warning","Incorrect AES key length must be 8 >> try again..")
            return

    elif Mode == "CaesarChiper":
        if not value_key.isnumeric():
            print("Incorrect CaesarChiper key must be only real numbers >> try again..")
            messagebox.showwarning("Warning","Incorrect CaesarChiper key must be only real numbers >> try again..")
            return

    elif Mode == "PlayFiar":
        if not value_key.isalpha():
            print("Incorrect PlayFiar key must be only chars >> try again..")
            messagebox.showwarning("Warning","Incorrect PlayFiar key must be only chars >> try again..")
            return
        
        if Type == "Text":
            if not value_text.isalpha():
                print("Incorrect PlayFiar text must be only chars >> try again..")
                messagebox.showwarning("Warning","Incorrect PlayFiar text must be only chars >> try again..")
                return
    
    if Type == "Text":
        if value_text == "":
            messagebox.showwarning("Warning","Please write text to Encrypt")
            return
    

        if Mode == "AES":
            EncryptionTools.AES_algorithm.set_key(value_key)
            EncryptionTools.AES_algorithm.set_msg(value_text)
            ciphertext = EncryptionTools.AES_algorithm.encrypt()
            text_Encypted_var.set(ciphertext)
            
        elif Mode == "DES":
            EncryptionTools.DES_algorithm.set_key(value_key)
            EncryptionTools.DES_algorithm.set_msg(value_text)
            ciphertext = EncryptionTools.DES_algorithm.encrypt()
            text_Encypted_var.set(ciphertext)

        elif Mode == "CaesarChiper":
            EncryptionTools.CaesarChiper_algorithm.set_key(value_key)
            EncryptionTools.CaesarChiper_algorithm.set_msg(value_text)
            ciphertext = EncryptionTools.CaesarChiper_algorithm.encrypt()
            text_Encypted_var.set(ciphertext)

        elif Mode == "VigeneresCipher":
            EncryptionTools.VigeneresCipher_algorithm.set_key(value_key)
            EncryptionTools.VigeneresCipher_algorithm.set_msg(value_text)
            ciphertext = EncryptionTools.VigeneresCipher_algorithm.encrypt()
            text_Encypted_var.set(ciphertext)

        elif Mode == "PlayFiar":
            EncryptionTools.PlayFiar_algorithm.set_key(value_key)
            EncryptionTools.PlayFiar_algorithm.set_msg(value_text)
            ciphertext = EncryptionTools.PlayFiar_algorithm.encrypt()
            text_Encypted_var.set(ciphertext)
    else:
        messagebox.showinfo("Information", "Choose the file containing the text to be encrypted")
        
        filetypes = (
        ('text files', '*.txt'),
        ('All files', '*.*')
        )
        filename = filedialog.askopenfilename(
            title='Open a file',
            filetypes=filetypes)
        
        if filename == "":
            return
        
        # messagebox.showinfo(
        # title='Selected File',
        # message=filename)
        
        if Mode == "AES":
            EncryptionTools.AES_algorithm.set_key(value_key)
            EncryptionTools.AES_algorithm.set_file(filename)
            EncryptionTools.AES_algorithm.encrypt()
            
        elif Mode == "DES":
            EncryptionTools.DES_algorithm.set_key(value_key)
            EncryptionTools.DES_algorithm.set_file(filename)
            EncryptionTools.DES_algorithm.encrypt()
        
        elif Mode == "CaesarChiper":
            EncryptionTools.CaesarChiper_algorithm.set_key(value_key)
            EncryptionTools.CaesarChiper_algorithm.set_file(filename)
            EncryptionTools.CaesarChiper_algorithm.encrypt()
            
        elif Mode == "VigeneresCipher":
            EncryptionTools.VigeneresCipher_algorithm.set_key(value_key)
            EncryptionTools.VigeneresCipher_algorithm.set_file(filename)
            EncryptionTools.VigeneresCipher_algorithm.encrypt()
        
        elif Mode == "PlayFiar":
            EncryptionTools.PlayFiar_algorithm.set_key(value_key)
            try:
                EncryptionTools.PlayFiar_algorithm.set_file(filename)
            except ValueError as e:
                e_str = str(e)
                messagebox.showwarning("Warning",e_str)
                return
            EncryptionTools.PlayFiar_algorithm.encrypt()

        messagebox.showinfo("Information", f"the file it's saved in the same program path as 'encrypted_{Mode}.bin'")

btn_Encryption = tk.Button(root,
        text="Encryption",
        command=btn_Encryption_command
        )
btn_Encryption.place(relx=0.5, rely=0.63, anchor=tk.CENTER)

l_EncryptType = tk.Label(root,
                text="Choose Encrypt type:",
                font=("Arial", 13),
                )
l_EncryptType.place(relx=0.05, rely=0.7)

Type="Text"
def selection_Type():
    global Type
    Type= radio_Type.get()
    
    if Type == "File":
        l_Encypted.place_forget()
        inp_Encypted.place_forget()
        btn_ClearEncryption.place_forget()
        l_text.config(state='disabled')
        inp_text.config(state='disabled')
        
    else:
        l_Encypted.place(relx=0.5, rely=0.8, anchor=tk.CENTER)
        inp_Encypted.place(relx=0.5, rely=0.86, anchor=tk.CENTER)
        btn_ClearEncryption.place(relx=0.5, rely=0.93, anchor=tk.CENTER)
        l_text.config(state=tk.NORMAL)
        inp_text.config(state=tk.NORMAL)


radio_Type = tk.StringVar()
rb_Text = tk.Radiobutton(root,
                        text="Text",
                        variable=radio_Type,
                        value="Text",
                        font=("Arial", 11),
                        command=selection_Type
                        )
rb_File = tk.Radiobutton(root,
                        text="File",
                        variable=radio_Type,
                        value="File",
                        font=("Arial", 11),
                        tristatevalue=0,
                        command=selection_Type
                        )

rb_Text.place(relx=0.5, rely=0.7)
rb_File.place(relx=0.7, rely=0.7)

l_Encypted = tk.Label(root,
                text="Encypted text",
                font=("Arial", 13),
                )
l_Encypted.place(relx=0.5, rely=0.8, anchor=tk.CENTER)

text_Encypted_var = tk.StringVar()

inp_Encypted = tk.Entry(root,
                width=60,
                textvariable=text_Encypted_var,
                state='readonly',
                disabledbackground="white",
                disabledforeground="black",
                readonlybackground="white",
                justify='center',
                )


inp_Encypted.config({"background": "White"})

inp_Encypted.place(relx=0.5, rely=0.86, anchor=tk.CENTER)

def btn_ClearEncryption_command():
    text_Encypted_var.set(value="")

btn_ClearEncryption = tk.Button(root,
        text="Clear",
        command=btn_ClearEncryption_command
        )
btn_ClearEncryption.place(relx=0.5, rely=0.93, anchor=tk.CENTER)

#root.mainloop()
