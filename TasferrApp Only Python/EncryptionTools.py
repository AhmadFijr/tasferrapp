from Crypto.Cipher import AES
from Crypto.Cipher import DES

class AES_algorithm:
    cipher = None
    def set_key(key):
        key = key.encode()
        try:
            AES_algorithm.cipher = AES.new(key, AES.MODE_EAX)
        except ValueError as e:
                print("Incorrect AES key length must be 16 >> try again..")

    msg = None
    def set_msg(text):
        AES_algorithm.msg = text

    def set_file(path):
        with open(path,"r") as f:
                AES_algorithm.msg = f.read()

    
    def encrypt(file=False):
        nonce = AES_algorithm.cipher.nonce
        ciphertext , tag = AES_algorithm.cipher.encrypt_and_digest(AES_algorithm.msg.encode('utf-8'))
        print("ciphertext AES:",ciphertext)
        
        #return nonce, ciphertext, tag
        if file:
            file_out = open("encrypted_AES.bin", "wb")
            [ file_out.write(x) for x in (AES_algorithm.cipher.nonce, tag, ciphertext) ]
            file_out.close()
        else:
            return ciphertext.hex()


class DES_algorithm:
    cipher = None
    def set_key(key):
        key = key.encode()
        try:
            DES_algorithm.cipher = DES.new(key, DES.MODE_EAX)
        except ValueError as e:
                print("Incorrect DES key length must be 8 >> try again..")

    msg = None
    def set_msg(text):
        DES_algorithm.msg = text

    def set_file(path):
        with open(path,"r") as f:
                DES_algorithm.msg = f.read()

    
    def encrypt(file=False):
        nonce = DES_algorithm.cipher.nonce
        ciphertext , tag = DES_algorithm.cipher.encrypt_and_digest(DES_algorithm.msg.encode('utf-8'))
        print("ciphertext DES:",ciphertext)

        #return nonce, ciphertext, tag
        if file:
            file_out = open("encrypted_DES.bin", "wb")
            [ file_out.write(x) for x in (DES_algorithm.cipher.nonce, tag, ciphertext) ]
            file_out.close()
        else:
            return ciphertext.hex()


class CaesarChiper_algorithm:
    key=None
    def set_key(key_inp):
        try:
            CaesarChiper_algorithm.key = int(key_inp)
        except ValueError :
            print("Incorrect CaesarChiper key must be only real numbers >> try again..")

    msg = None
    def set_msg(text):
        CaesarChiper_algorithm.msg = text

    def set_file(path):
        with open(path,"r") as f:
                CaesarChiper_algorithm.msg = f.read()

    def encrypt(file=False):
        res = ""
        for i in range(len(CaesarChiper_algorithm.msg)):
            char = CaesarChiper_algorithm.msg[i]
            s = chr((ord(char) - 65 + CaesarChiper_algorithm.key) % 26 + 65)
            res += s
        print("res:",res)
        if file:
            file_out = open("encrypted_CaesarChiper.bin", "w")
            file_out.write(res)
            file_out.close()
        else:
            return res

class VigeneresCipher_algorithm:
    def __generateKey(string, key):
        key = list(key)
        if len(string) == len(key):
            return(key)
        else:
            for i in range(len(string) -
                        len(key)):
                key.append(key[i % len(key)])
        return("" . join(key))

    def __cipherText(string, key):
        cipher_text = []
        for i in range(len(string)):
            x = (ord(string[i]) +
                ord(key[i])) % 26
            x += ord('A')
            cipher_text.append(chr(x))
        return("" . join(cipher_text))
    
    keyword=None
    def set_key(key_inp):
        VigeneresCipher_algorithm.keyword = key_inp


    msg = None
    def set_msg(text):
        VigeneresCipher_algorithm.msg = text

    def set_file(path):
        with open(path,"r") as f:
            VigeneresCipher_algorithm.msg = f.read()

    def encrypt(file=False):
        key = VigeneresCipher_algorithm.__generateKey(VigeneresCipher_algorithm.msg, VigeneresCipher_algorithm.keyword)
        cipher_text = VigeneresCipher_algorithm.__cipherText(VigeneresCipher_algorithm.msg,key)
        print("Ciphertext :", cipher_text)


        if file:
            file_out = open("encrypted_VigeneresCipher.bin", "w")
            file_out.write(cipher_text)
            file_out.close()
        else:
            return cipher_text


class PlayFiar_algorithm:
    def __convertPlainTextToDiagraphs (plainText):
        # append X if Two letters are being repeated
        for s in range(0,len(plainText)+1,2):
            if s<len(plainText)-1:
                if plainText[s]==plainText[s+1]:
                    plainText=plainText[:s+1]+'X'+plainText[s+1:]

        # append X if the total letters are odd, to make plaintext even
        if len(plainText)%2 != 0:
            plainText = plainText[:]+'X'

        return plainText
    
    def __generateKeyMatrix(key): 
        # Intially Create 5X5 matrix with all values as 0
        # [
        #   [0, 0, 0, 0, 0],
        #   [0, 0, 0, 0, 0], 
        #   [0, 0, 0, 0, 0], 
        #   [0, 0, 0, 0, 0], 
        #   [0, 0, 0, 0, 0]
        # ]
        matrix_5x5 = [[0 for i in range (5)] for j in range(5)]


        simpleKeyArr = []


        """
        Generate SimpleKeyArray with key from user Input 
        with following below condition:
        1. Character Should not be repeated again
        2. Replacing J as I (as per rule of playfair cipher)
        """
        for c in key:
            if c not in simpleKeyArr:
                if c == 'J':
                    simpleKeyArr.append('I')
                else:
                    simpleKeyArr.append(c)


        """
        Fill the remaining SimpleKeyArray with rest of unused letters from english alphabets 
        """

        is_I_exist = "I" in simpleKeyArr

        # A-Z's ASCII Value lies between 65 to 90 but as range's second parameter excludes that value we will use 65 to 91
        for i in range(65,91):
            if chr(i) not in simpleKeyArr:
                # I = 73
                # J = 74
                # We want I in simpleKeyArr not J


                if i==73 and not is_I_exist:
                    simpleKeyArr.append("I")
                    is_I_exist = True
                elif i==73 or i==74 and is_I_exist:
                    pass
                else:
                    simpleKeyArr.append(chr(i))

        """
        Now map simpleKeyArr to matrix_5x5 
        """
        index = 0
        for i in range(0,5):
            for j in range(0,5):
                matrix_5x5[i][j] = simpleKeyArr[index]
                index+=1



        return matrix_5x5

    def __indexLocator (char,cipherKeyMatrix):
        indexOfChar = []

        # convert the character value from J to I
        if char=="J":
            char = "I"

        for i,j in enumerate(cipherKeyMatrix):
            # enumerate will return object like this:         
            # [
            #   (0, ['K', 'A', 'R', 'E', 'N']),
            #   (1, ['D', 'B', 'C', 'F', 'G']), 
            #   (2, ['H', 'I', 'L', 'M', 'O']), 
            #   (3, ['P', 'Q', 'S', 'T', 'U']), 
            #   (4, ['V', 'W', 'X', 'Y', 'Z'])
            # ]
            # i,j will map to tupels of above array


            # j refers to inside matrix =>  ['K', 'A', 'R', 'E', 'N'],
            for k,l in enumerate(j):
                # again enumerate will return object that look like this in first iteration: 
                # [(0,'K'),(1,'A'),(2,'R'),(3,'E'),(4,'N')]
                # k,l will map to tupels of above array
                if char == l:
                    indexOfChar.append(i) #add 1st dimension of 5X5 matrix => i.e., indexOfChar = [i]
                    indexOfChar.append(k) #add 2nd dimension of 5X5 matrix => i.e., indexOfChar = [i,k]
                    return indexOfChar


    def __encryption (plainText,key):
        cipherText = []
        # 1. Generate Key Matrix
        keyMatrix = PlayFiar_algorithm.__generateKeyMatrix(key)

        # 2. Encrypt According to Rules of playfair cipher
        i = 0
        while i < len(plainText):
            # 2.1 calculate two grouped characters indexes from keyMatrix
            n1 = PlayFiar_algorithm.__indexLocator(plainText[i],keyMatrix)
            n2 = PlayFiar_algorithm.__indexLocator(plainText[i+1],keyMatrix)

            # 2.2  if same column then look in below row so
            # format is [row,col]
            # now to see below row => increase the row in both item
            # (n1[0]+1,n1[1]) => (3+1,1) => (4,1)

            # (n2[0]+1,n2[1]) => (4+1,1) => (5,1)

            # but in our matrix we have 0 to 4 indexes only
            # so to make value bound under 0 to 4 we will do %5
            # i.e.,
            #   (n1[0]+1 % 5,n1[1])
            #   (n2[0]+1 % 5,n2[1])

            if n1[1] == n2[1]:
                i1 = (n1[0] + 1) % 5
                j1 = n1[1]

                i2 = (n2[0] + 1) % 5
                j2 = n2[1]
                cipherText.append(keyMatrix[i1][j1])
                cipherText.append(keyMatrix[i2][j2])
                cipherText.append(", ")

            # same row
            elif n1[0]==n2[0]:
                i1= n1[0]
                j1= (n1[1] + 1) % 5


                i2= n2[0]
                j2= (n2[1] + 1) % 5
                cipherText.append(keyMatrix[i1][j1])
                cipherText.append(keyMatrix[i2][j2])
                cipherText.append(", ")


            # if making rectangle then
            # [4,3] [1,2] => [4,2] [3,1]
            # exchange columns of both value
            else:
                i1 = n1[0]
                j1 = n1[1]

                i2 = n2[0]
                j2 = n2[1]

                cipherText.append(keyMatrix[i1][j2])
                cipherText.append(keyMatrix[i2][j1])
                cipherText.append(", ")

            i += 2  
        return cipherText

    key=None
    def set_key(key_inp):
        if not key_inp.isalpha():
            print("Incorrect PlayFiar key must be only chars >> try again..")
        else:
            key_inp=key_inp.replace(" ","").upper()
            PlayFiar_algorithm.key = key_inp

    msg = None
    def set_msg(text):
        if not text.isalpha():
            print("Incorrect PlayFiar text must be only chars >> try again..")
        else:
            text=text.replace(" ","").upper()
            PlayFiar_algorithm.msg = text

    def set_file(path):
        with open(path,"r") as f:
            read = f.read()
            if not read.isalpha():
                print("Incorrect PlayFiar text must be only chars >> try again..")
                raise ValueError("Incorrect PlayFiar text must be only chars >> try again..")
            else:
                read=read.replace(" ","").upper()
                PlayFiar_algorithm.msg = read

    def encrypt(file=False):
        convertedPlainText = PlayFiar_algorithm.__convertPlainTextToDiagraphs(PlayFiar_algorithm.msg)
        
        cipherText = "".join(PlayFiar_algorithm.__encryption(convertedPlainText,PlayFiar_algorithm.key))
        cipherText = cipherText.replace(",", "").replace(" ", "")
        print("Ciphertext :", cipherText)

        if file:
            file_out = open("encrypted_PlayFiar.bin", "w")
            file_out.write(cipherText)
            file_out.close()
        else:
            return cipherText